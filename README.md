# MaSys Slack incoming webhook integration

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'MSlack', git: 'https://yalmasri@bitbucket.org/yalmasri/mslack.git'
```

And then execute:

```bash
$ bundle
```

Or install it yourself as:

```bash
$ git clone https://yalmasri@bitbucket.org/yalmasri/mslack.git && cd mslack
$ gem install MSlack
```

## Usage

You can setup default configuration for **Slack** class:

```ruby
MSlack::Incoming.configure do |config|
  config.incoming_webhook_url: 'slack_url'
  config.username = 'your username'
  config.channel = '#channel or @username'
end
```

and then you can initialize **Slack**:

```ruby
slack = MSlack::Incoming.new()
```

or you can specify your params like this:

```ruby
slack = MSlack::Incoming.new(
  incoming_webhook_url = 'slack_url',
  channel: '#general',
  username: 'your_username'
)
```

and then to post a message to **Slack**:

```ruby
slack.push(messgae: 'my message')
```

You can add attachemnts to the message this way:

```ruby
slack.push(message: 'my message', attachments: [{ text: 'your text', color: '#CC0000' }])
```

You can find [Here](https://api.slack.com/docs/attachments) more params to includes in 'attachments' array.

to check if the message has been posted successfully, you can read the response with:

```ruby
slack.response.body
```

it must retun 'ok'.