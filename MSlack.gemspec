# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'MSlack/version'

Gem::Specification.new do |spec|
  spec.name          = "MSlack"
  spec.version       = MSlack::VERSION
  spec.authors       = ["Yaser Almasri"]
  spec.email         = ["info@masys.co"]

  spec.summary       = %q{Slack integration from MaSys Development.}
  spec.description   = %q{Slack Integration, to send messages to your slack team.}
  spec.homepage      = "http://www.masys.co"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  # spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.files         = Dir['lib/**/*.rb']
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_dependency "rest-client", "~> 2.0.0"
  spec.add_dependency "json", "~> 1.8.3"
end
