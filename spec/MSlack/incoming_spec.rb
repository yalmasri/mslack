require 'spec_helper'

describe MSlack::Incoming do
  before do
    @url = 'https://hooks.slack.com/services/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'
  end
  describe "#configure" do
    context 'with default configurations' do
      before do
        MSlack::Incoming.configure do |config|
          config.incoming_webhook_url = @url
        end
      end

      it "returns an incoming webhook url" do
        expect(MSlack::Incoming.configuration.incoming_webhook_url).to eq(@url)
      end

      it 'posts a message to slack' do
        slack = MSlack::Incoming.new()
        slack.push(message: 'testing message')
        expect(slack.response.body).to eq('ok')
      end
    end

    context 'with specific configurations' do
      before do
        MSlack::Incoming.configure do |config|
          config.incoming_webhook_url = @url
          config.username = 'my_username'
          config.channel = '#random'
        end
      end

      it 'posts a message to slack' do
        slack = MSlack::Incoming.new()
        slack.push(message: 'testing message')
        expect(slack.response.body).to eq('ok')
      end
    end

    context 'with specific arguments' do
      before do
        MSlack::Incoming.configure do |config|
          config.incoming_webhook_url = @url
        end
      end

      it 'posts a message to slack' do
        slack = MSlack::Incoming.new(username: 'my_username', channel: '#random')
        slack.push(message: 'testing message')
        expect(slack.response.body).to eq('ok')
      end
    end
  end
end
