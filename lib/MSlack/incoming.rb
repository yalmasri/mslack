require 'active_support/all'
require 'ostruct'
require 'json'
require 'rest-client'

module MSlack
  class Incoming
    # reader attribute to read response after sending message
    attr_reader :response

    # Class methods to set default configurations
    class << self
      attr_writer :configuration
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.reset
      @configuration = Configuration.new
    end

    def self.configure
      yield(configuration)
    end

    # initializes class and sets arguments
    def initialize(args = {})
      @url = args[:incoming_webhook_url] || default_incomig_webhook
      @channel = args[:channel] || default_channel
      @username = args[:username] || default_username

      fail ArgumentError, 'You need to specify a slack url' unless @url.present?
    end

    # posts a message to slack and sets response
    def push(args = {})
      @text = args[:message]
      @attachments = args[:attachments]
      @response = RestClient.post @url, params.to_json,
        content_type: :json, accept: :json
    end

    private

    def params
      {
        channel: @channel, username: @username,
        text: @text, attachments: @attachments
      }
    end

    # default configuration for slack
    def default_incomig_webhook
      Incoming.configuration.incoming_webhook_url
    end

    def default_channel
      Incoming.configuration.channel || '#general'
    end

    def default_username
      Incoming.configuration.username || 'webhookbot'
    end
  end
end
